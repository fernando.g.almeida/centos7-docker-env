### Testing docker-machine with centos7 virtualbox machine!


#### First use of docker-machine 

- copy ssh key

>   $ ssh-copy-id dockeradmin@192.168.33.10
>      < pass ptin_admin >

- create vm machine

>   $ docker-machine create --driver generic --generic-ip-address=192.168.33.10 --generic-ssh-user dockeradmin --generic-ssh-key ~/.ssh/id_rsa vm

- start env to use docker

>   $ eval $(docker-machine env vm)

- verify if there is no proxy defined or set no_proxy for 192.168.33.0/24

>   $ unset http_proxy
>   $ unset https_proxy

- this must work 

>   $ docker images
>   REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE

