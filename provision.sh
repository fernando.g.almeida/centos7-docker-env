#!/usr/bin/bash

sudo mv ./docker-main.repo /etc/yum.repos.d/docker-main.repo
sudo mv ./ZscalerRootCA-ZscalerInc-dockeruse.der /etc/pki/ca-trust/source/anchors/ZscalerRootCA-ZscalerInc-dockeruse.der
sudo chown root.root ./dockeradmin
sudo mv ./dockeradmin /etc/sudoers.d/dockeradmin

sudo update-ca-trust extract
sudo update-ca-trust

sudo yum -y install net-tools docker-engine

sudo systemctl stop firewalld
sudo systemctl disable firewalld

ptinpass=`sudo grep dockeradmin /etc/shadow`
if [ "$ptinpass" = "" ]
then 
   pass=`sudo awk -F ':' '$1 == "ptin_admin" {print $2}' /etc/shadow`
   sudo useradd -G docker -p "$pass" dockeradmin  && echo "User dockeradmin created ... "
   grep dockeradmin /etc/shadow
fi
